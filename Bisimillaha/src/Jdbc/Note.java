

@Entity
public class Note {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)  
    private int dept_no;
	private String d_name;
	private String d_email;
	
	
    @OneToMany(targetEntity=Same.class)
	public Note(int dept_no, String d_name) {
		super();
		this.dept_no = dept_no;
		this.d_name = d_name;
	}

	public Note() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getDeptno() {
		return dept_no;
	}
	public void setDeptno(int deptno) {
		this.dept_no = deptno;
	}
	public String getDeptname() {
		return d_name;
	}
	public void setDeptname(String deptname) {
		this.d_name = deptname;
	}
	}
	
