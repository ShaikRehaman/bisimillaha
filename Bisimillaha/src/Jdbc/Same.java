import java.util.HashSet;
import java.util.Set;

@Entity
public class Same {
	
    @Id
    @GeneratedValue(Strategy=GenerationType.AUTO)
	private int e_no;
	private String e_name;
	
	@ManyToOne(targetEntity=Note.class)
	@joinColumn(name = "dept_no",referencedColumnname ="Id")
	private Set<Note>Note = new HashSet<Note>();
	
	
	public int getEno() {
		
		return e_no;
	}
	public void setEno(int eno) {
		this.e_no = eno;
	}
	public String getEname() {
		return e_name;
	}
	public void setEname(String ename) {
		this.e_name = ename;
	}
}
	
	
	

