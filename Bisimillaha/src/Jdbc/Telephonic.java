import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Telephonic {

	// import java.sql.*;

	public static void main(String args[]) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://sql742.main-hosting.eu:3306/u490063748_java?useSSL=false", "u490063748_java",
					"$Atyam123");
			// here sonoo is database name, root is username and password
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from Java");
			// stmt.executeUpdate("UPDATE Java SET FirstName='Sajid' where PersonId=7");
			stmt.execute("Insert into Java values(6,'Sk','IMRAN','MaguntaLayout','Hyderabad',20000,'Ravi')");
			// stmt.execute("Delete from Java where PersonID=6");

			/*
			 * PreparedStatement
			 * st=con.prepareStatement("Update Java SET FirstName='Dhoni' where PersonId=5"
			 * ); st.executeUpdate();
			 */
			/*
			 * PreparedStatement st1=con.
			 * prepareStatement("Insert into Java values(7,'Ms','DhoniSingh','Richnagar','Jarkhand',50000,'BCCI')"
			 * ); st1.execute();
			 */
			/*
			 * PreparedStatement
			 * st2=con.prepareStatement("Delete from Java where PersonId=7 ");
			 * st2.execute();
			 */
			while (rs.next())
				System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
