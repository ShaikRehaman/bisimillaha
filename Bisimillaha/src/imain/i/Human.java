package i;

public interface Human {

	void talk();

	void walk();

	void eat();

	void sleep();
}

class Man implements Human {
	public void talk() {
		System.out.println("A man can talk");
	}

	public void walk() {
		System.out.println("A man can walk");
	}

	public void eat() {
		System.out.println("A man can eat");
	}

	public void sleep() {
		System.out.println("A man can sleep");
	}
}

class Boy extends Man {

	public void talk() {
		System.out.println("A Boy can talk");
	}

	public void walk() {
		System.out.println("A Boy can walk");
	}

	public void eat() {
		System.out.println("A Boy can eat");
	}

	public void sleep() {
		System.out.println("A Boy can sleep");

	}

}

class Woman implements Human {

	public void talk() {
		System.out.println("A Woman can talk");
	}

	public void walk() {
		System.out.println("A  Woman can walk");
	}

	public void eat() {
		System.out.println("A Woman  can eat");
	}

	public void sleep() {
		System.out.println("A Woman can sleep");

	}

}

class Girl extends Woman {
	public void talk() {
		System.out.println("A Girl can talk");
	}

	public void walk() {
		System.out.println("A  Girl can walk");
	}

	public void eat() {
		System.out.println("A Girl  can eat");
	}

	public void sleep() {
		System.out.println("A Girl can sleep");

	}

}

class BabyBoy extends Boy {
	public void talk() {
		System.out.println("A BabyBoy can talk");
	}

	public void walk() {
		System.out.println("A  Baby boy can walk");
	}

	public void eat() {
		System.out.println("A Baby boy  can eat");
	}

	public void sleep() {
		System.out.println("A Baby boy can sleep");

	}

}

class Babygirl extends Girl {
	public void talk() {
		System.out.println("A BabyGirl can talk");
	}

	public void walk() {
		System.out.println("A  BabyGirl can walk");
	}

	public void eat() {
		System.out.println("A BabyGirl  can eat");
	}

	public void sleep() {
		System.out.println("A BabyGirl can sleep");

	}

}

class Babykid extends Girl {
	public void talk() {
		System.out.println("A Babykid can talk");
	}

	public void walk() {
		System.out.println("A  Babykid can walk");
	}

	public void eat() {
		System.out.println("A Babykid  can eat");
	}

	public void sleep() {
		System.out.println("A Babykid can sleep");

	}
}
