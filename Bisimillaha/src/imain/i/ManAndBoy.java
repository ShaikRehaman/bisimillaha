package i;

public class ManAndBoy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Human human = new Man();
		human.talk();
		human.walk();
		human.eat();
		human.sleep();

		human = new Boy();
		human.talk();
		human.walk();
		human.eat();
		human.sleep();

		human = new Woman();
		human.talk();
		human.walk();
		human.eat();
		human.sleep();

		human = new Girl();
		human.talk();
		human.walk();
		human.eat();
		human.sleep();

		human = new BabyBoy();
		human.talk();
		human.walk();
		human.eat();
		human.sleep();

		human = new Babygirl();
		human.talk();
		human.walk();
		human.eat();
		human.sleep();

		human = new Babykid();
		human.talk();
		human.walk();
		human.eat();
		human.sleep();

	}

}
