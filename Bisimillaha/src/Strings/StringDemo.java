package Strings;

public class StringDemo {
	public static void main(String[] args) {
		
		String s1 =new String("Hello World");
		String s2 ="Welcome to Java";
		
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s1.length());
		System.out.println(s2.length());
	}

}
