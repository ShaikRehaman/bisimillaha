package Highlevelpackage;

public class EmployeeDetails {
	
	private String Name;
	private int id;
	private long mobilenumber;
	private String emailid;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(long mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	@Override
	public String toString() {
		return "EmployeeDetails [Name=" + Name + ", id=" + id + ", mobilenumber=" + mobilenumber + ", emailid="
				+ emailid + ", getName()=" + getName() + ", getId()=" + getId() + ", getMobilenumber()="
				+ getMobilenumber() + ", getEmailid()=" + getEmailid() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
	
    public void prindetails() {
		
		System.out.println("name:" +Name);
		System.out.println("id:" +id);
		System.out.println("mobilenumber:" +mobilenumber);
		System.out.println("emailid:" +emailid);
	

	}

}
