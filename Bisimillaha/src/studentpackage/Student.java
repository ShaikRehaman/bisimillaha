package studentpackage;

public class Student {
	
	 int PASSMARKS = 35;
	 private int rollno;
	 
	 private String name;
	 
	 private int m1;
	 private int physics;
	 private int chemistry;
	 
	 public int getPASSMARKS() {
		  return PASSMARKS;
	 }

	public int getRollno() {
		return rollno;
	}

	public void setRollno(int rollno) {
		this.rollno = rollno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getM1() {
		return m1;
	}

	public void setM1(int m1) {
		this.m1 = m1;
	}

	public int getPhysics() {
		return physics;
	}

	public void setPhysics(int physics) {
		this.physics = physics;
	}

	public int getChemistry() {
		return chemistry;
	}

	public void setChemistry(int chemistry) {
		this.chemistry = chemistry;
	}

	public int totalmarks() {
		return m1+physics+chemistry;
    }
	
	public int percentage() {
		return totalmarks()/3;
	}
	public boolean ispassed() {
		return m1>=PASSMARKS && physics>=PASSMARKS &&chemistry>=PASSMARKS;
	}
		
	public void setPASSMARKS(int pASSMARKS) {
		PASSMARKS = pASSMARKS;
	}

	public String getGrade() {
		
		if(ispassed()) {
			return "failed";
		}
		if(percentage() >75) {
			return "Distinction";
		}
		if(percentage() >60) {
			return "first class";
		}
		if(percentage() >50) {
			return "second class";
		}else {
			return "third class";
		}
	}

	@Override
	public String toString() {
		return "Student [Passmarks=" + PASSMARKS + ", rollno=" + rollno + ", name=" + name + ", m1=" + m1 + ", physics="
				+ physics + ", chemistry=" + chemistry + ", getPassmarks()=" + getPASSMARKS() + ", getRollno()="
				+ getRollno() + ", getName()=" + getName() + ", getM1()=" + getM1() + ", getPhysics()=" + getPhysics()
				+ ", getChemistry()=" + getChemistry() + ", totalmarks()=" + totalmarks() + ", percentage()="
				+ percentage() + ", ispassed()=" + ispassed() + ", getGrade()=" + getGrade() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	public void prindetails() {
		
		System.out.println("name:" +name);
		System.out.println("rollno:" +rollno);
		System.out.println("m1:" +m1);
		System.out.println("physics:" +physics);
		System.out.println("chemistry:" +chemistry);
		System.out.println("totalmarks:" +totalmarks());
		System.out.println("percentage:" +percentage());
	    System.out.println("getGrade:" +getGrade());
	}
		
		
	
	 
	 
	 

}
