package constructers;

import Rehaman2.Boy;

public class Human extends Boy {
	private int age;
	private String gender;
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Human(String name, int mobile, int age, String gender) {
		super(name, mobile);
		this.age = age;
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Human [age=" + age + ", gender=" + gender + ", name=" + name + ", mobile=" + mobile + "]";
	}
		
		//Parametirsed Constructor
		//public Human(int age, String gender) {
			//super();
			//this.age = age;
			//this.gender = gender;
		//}
		//@Override
		//public String toString() {
			/*return "Human [age=" + age + ", gender=" + gender + ", getAge()=" + getAge() + ", getGender()="
					+ getGender() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
					+ super.toString() + "]";
		}*/
		//Default constructer
				//public Human() {
		
		
				}
	

//}
