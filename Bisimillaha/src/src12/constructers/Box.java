package constructers;

public class Box {

	@Override
	public String toString() {
		return "Box [length=" + length + ", width=" + width + "]";
	}


	private int length;
	private int width;

	
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

//	default constuctor
	public Box() {
	
	}

//	parameterizised constructor
	public Box(int length, int width) {
		this.length = length;
		this.width = width;
	}
	
	
	public int getVolume() {
		return length * width;
	}

}
