package constructers;

public class NelloreText extends Human{
	
	private int height;
	private String name;
	
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		
	}
	public NelloreText(int age,String gender,int height, String name) {
		super(name, age,height, gender);
		this.height = height;
		this.name = name;
	}
	
	
	@Override
	public String toString() {
		return "NelloreText [height=" + height + ", name=" + name + ", getHeight()=" + getHeight() + ", getName()="
				+ getName() + ", getAge()=" + getAge() + ", getGender()=" + getGender() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	//public NelloreText() {
	
	}



