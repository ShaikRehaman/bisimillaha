import java.util.Objects;

public class Apple implements Comparable<Apple> {
	
private int Id;
private String Name;
private String Emailid;
private int Salary;

public int getId() {
	return Id;
}
public void setId(int id) {
	Id = id;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
public String getEmailid() {
	return Emailid;
}
public void setEmailid(String emailid) {
	Emailid = emailid;
}
public int getSalary() {
	return Salary;
}
public void setSalary(int salary) {
	Salary = salary;
}
public Apple(int id, String name, String emailid, int salary) {
	super();
	Id = id;
	Name = name;
	Emailid = emailid;
	Salary = salary;
}

@Override
public int hashCode() {
	return Objects.hash(Emailid, Name);
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Apple other = (Apple) obj;
	return Objects.equals(Emailid, other.Emailid) && Objects.equals(Name, other.Name);
}
@Override
public String toString() {
	return "Apple [Name=" + Name + ", Emailid=" + Emailid + "]";
}
@Override
public int compareTo(Apple o) {
	// TODO Auto-generated method stub
	return this.getName().compareTo(o.getName());
}


	
}
